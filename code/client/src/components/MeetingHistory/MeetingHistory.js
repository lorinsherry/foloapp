import React, { useState, useEffect } from "react";
import { BackButton } from "./BackButton";
import { IconAndDetails } from "./IconAndDetails";
import { CurrMeetingDetails } from "./CurrMeetingDetails";
import { Meeting } from "./Meeting";
import { ProgressBar } from "./ProgressBar";
import { Form } from "reactstrap";
import { useParams } from "react-router";
import { FoloAppService } from "../../service/api";

export const MeetingHistory = (props) => {
  
  const [currMeeting, setCurrMeeting] = useState({});
  const [meetings, setMeetings] = useState([]);
  const [meetingNumber, setMeetingNumber] = useState(0);


  let { id } = useParams();

  const changeCurrMeeting = (data, meetingNumber) => {
    setMeetingNumber(meetingNumber);
    setCurrMeeting(data);
  };

  useEffect(() => {
    FoloAppService.getAllMeetings_by_id_user(id).then((response) => {
      setMeetings(response);
    });
  }, []);

  

  // let totalAmountOfMeetings = props.meetings.length;
  // let doneMeetings = 1;//Find all the meetings that done. *Search for the right Array methods.
  return (
    <Form>
      <BackButton />
       <IconAndDetails userId={id} />

      {/* <ProgressBar /> */}
      <div className="meetings_container">
        <div className="meetings">
          {meetings.map((meeting, index) => (
            <Meeting
              key={meeting.pk}
              data={meeting}
              meetingNumber={index + 1}
              changeCurrMeeting={changeCurrMeeting}
            />
          ))}

          <button className="timelineCurr">
            <h1 id="plus">+</h1>
          </button>
        </div>
      </div>
      <div>
        {Object.keys(currMeeting).length === 0 ? null : (
          <CurrMeetingDetails
            selectedMeeting={currMeeting}
            meetingNumber={meetingNumber}
          />
        )}
      </div>
    </Form>
  );
};

import React from "react";
import { Progress } from "reactstrap";

export const ProgressBar = (props) => {
  //let total = (100 * {props.doneMeetings}) / total;
  return (
    <div>
      <div>
        <div className="text-center">
          <b>5/11</b>
        </div>
        <div className="Progress">
          <Progress value={45} />
        </div>
      </div>
    </div>
  );
};

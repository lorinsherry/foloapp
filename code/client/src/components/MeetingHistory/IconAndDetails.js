import React, { useState, useEffect } from "react";
import { Media } from "reactstrap";
import { FoloAppService } from "../../service/api";

export const IconAndDetails = (props) => {
 const [patient, setPatient] = useState({});

  useEffect(()=>{
    FoloAppService.getAllPatients().then((response) => {
        const data = JSON.parse(response);
        for (let i in data) {
          if ( props.userId == data[i].pk) {
           setPatient(data[i]);
          }
        }
  
      });
  },[])

  console.log(patient)
  return (
    <div>
      <div>
    

      </div>

      <Media body>
        <Media>
          <Media id="iconStyle" props src={Object.keys(patient).length !== 0 ? patient.fields.image_icon : null} alt="avatar" />
        </Media>
     
    
        <div className="pd">
          <div id="t">
            <span id="PerInfo">מטופל: </span>
            <span id="PerDet">
              {Object.keys(patient).length !== 0 ? patient.fields.name : null}
            </span>
          </div>
          <div id="t">
            <span id="PerInfo">גיל:  </span>

            <span id="PerDet">
            {Object.keys(patient).length !== 0 ? patient.fields.age : null}
            </span>
          </div>
          <div id="t">
            <span id="PerInfo">מין:  </span>

            <span id="PerDet">
            {Object.keys(patient).length !== 0 ? patient.fields.gender : null}
            </span>
          </div>
          <div id="t">
            <span id="PerInfo">איש קשר:</span>

            <span id="PerDet">
            {Object.keys(patient).length !== 0 ? patient.fields.name_contact_person : null}   0{Object.keys(patient).length !== 0 ? patient.fields.contact_person : null}
            </span>
            <div id="contact">צור קשר</div>
          </div>
        </div>
      </Media>
    </div>
  );
};

import React from 'react'
import "./MeetingHistory.css";

export const CurrMeetingDetails = (props) => {


    return (
        <div>
            <div className="AllMeet">
            <div className="descriptionMeet">
            <div className="descMeetHead"> מפגש מס: {props.meetingNumber}
            </div>
            </div>
            <div className="descMeet">
            {props.selectedMeeting.fields.description_mission}
            </div>
            </div>
                <div className="timeline">
                <div className="container right">
                <div className="contentTitle">
                {props.selectedMeeting.fields.date}
                <div>
                {props.selectedMeeting.fields.report === false ? 
                <div>לא הצליח {props.selectedMeeting.fields.meeting_date}
                    <div className="circle" style={{backgroundColor:'red'}}></div> 
                </div> :
                <div>הצליח   {props.selectedMeeting.fields.meeting_date}
                    <div className="circle" style={{backgroundColor:'green'}}></div> 
                </div> }
                
                               
                </div>
                </div>
                <p>{props.selectedMeeting.fields.instruction}</p>
            </div>
        </div>
    </div>
    
        
        
    )
}

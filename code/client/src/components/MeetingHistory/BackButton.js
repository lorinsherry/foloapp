import React from 'react'
import { useHistory } from 'react-router';
export const BackButton = () => {
    const history = useHistory();
    return (
        <div>
            <div id="back" onClick={()=>history.push('/PatientsList')}>
            {'>'}
            </div>
        </div>
    )
}



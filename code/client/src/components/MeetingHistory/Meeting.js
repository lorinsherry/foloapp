import React, { useState } from "react";
import "./Meeting.css";

export const Meeting = (props) => {
//   const [completeMeeting, setCompleteMeeting] = useState(false);

//   const changeCompleteMeeting = () => {
//     setCompleteMeeting(!completeMeeting);
//     console.log(completeMeeting);
//   };

  return (
    <div
      className="timelineCurr"
      onClick={() => props.changeCurrMeeting(props.data, props.meetingNumber)}
    >
      <div>
        מפגש מס: {props.meetingNumber}
      </div>
      <div className="VBtn"
        // className={completeMeeting ? "whiteCircle" : "whiteCircleChange"}
        // onClick={changeCompleteMeeting}
      >
        {props.data.fields.report
         ? (
          <div className="whiteCircle">V</div>
        ) : (
          <div className="whiteCircleChange"></div>
        )}
      </div>
      {props.data.fields.meeting_date}
      <div>
        <div></div>
      </div>
    </div>
  );
};

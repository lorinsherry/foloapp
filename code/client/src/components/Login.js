import React from "react";
import "./Login.css";
import { Link } from 'react-router-dom';

export const Login = () => {
  return (

    <div>
      <div className="Header">FoloApp</div>

      <div>
        <div>
        <input className="formD" type="text" placeholder="שם משתמש:" name="uname" required></input>
        </div>

        <div>
        <input className="formD" type="text" placeholder="סיסמא:" name="pswrd" required></input>
        </div>

      </div>

      <div className="login">
      <Link to="/PatientsList" type="submit">כניסה</Link>
      </div>
    </div>
    
  );
};

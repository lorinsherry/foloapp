import React, { useEffect, fetchUser, useState } from "react";
import { CurrPatientDetails } from "./CurrPatientDetails";
import "./Patient.css";
import { FoloAppService } from "../../service/api";
import { useHistory } from "react-router";
import { useParams } from "react-router";


export const Patient = (props) => {
  //console.log(props.data.fields.contact_person);
  const [showDetails, setShowDetails] = useState(false);
  const [meetingNum, setMeetingNum] = useState({});


  const history = useHistory();

  const changeShowDetails = () => {
    setShowDetails(!showDetails);
  };

  // let { id } = (props.pk);
  
  console.log(Object.keys(props).data);
  // useEffect(() => {
  //   FoloAppService.getAllMeetings_by_id_user(props.).then((response) => {
  //     setMeetingNum(response);
  //   });
  // }, []);

  
  
  return (
    <div className="patientCurr">
      <div
        className={showDetails ? "trianArrow" : "rotateArrow"}
        onClick={changeShowDetails}
      ></div>

      <div>
        <h5 style={{fontWeight:"700"}}> {props.data.fields.name}</h5>
        <p> {props.data.fields.gender == "נקבה" ? "ביצעה תרגול" : "ביצע תרגול"} <img src={"https://image.flaticon.com/icons/png/512/716/716225.png"} style={{width:"15px",height:"15px"}}></img></p>
        <p>טיפול מספר: 
          
           </p>
        <p>טיפול קרוב: להוסיף</p>
      </div>
      <div className="avatarAndPr">
        <img className="AvatarImageDetail" src={props.data.fields.image_icon} />
        <p>
          <div
            className="toProfile"
            onClick={() => history.push(`/MeetingHistory/${props.data.pk}`)}
          >
            {" "}
            לפרופיל
          </div>
        </p>
      </div>
      <div></div>

      <div>{showDetails && <CurrPatientDetails />}</div>
    </div>
  );
};

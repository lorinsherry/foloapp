import React from "react";
import "./Patient.css";

export const CurrPatientDetails = (props) => {
  

  return (
    <div className="contactBtnDisplay">
      {props.selectedPatient && props.selectedPatient.pk}
   
      <div className="smallBtnFirst" >
        <img src={"https://i.ibb.co/MBgLGQm/call.png"} alt="" className="iconImageDetail" />
        <div></div>
        טלפון
      </div>
      <div className="smallBtnSecond">
        <img src={"https://i.ibb.co/8Psnj5X/message.png"} alt="" className="iconImageDetail" />
        הודעות
      </div>
    </div>
  );
};

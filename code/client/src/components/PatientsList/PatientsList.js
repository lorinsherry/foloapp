import React, {useEffect,useState } from "react";
import { Patient } from "./Patient";
import { CurrPatientDetails } from "./CurrPatientDetails";
import { useHistory } from 'react-router-dom';
import { AddPatient } from "./AddPatient";
import './Patient.css'

export const PatientsList = (props) => {
  
  const [currPatient, setCurrPatient] = useState({});

  const changeCurrPatient = (data) => {
    setCurrPatient(data);
  };
 

  return (
    
    <div className="patients_container">
      <div className="patients">

        <div className="HeaderFront">
        <div className="headerP">
          רשימת מטופלים({props.patients.length})
          <div className="cssCircle plusSign" >+</div>
        </div>
        </div>
        
        <div className="searchBar">
          <div className="fa fa-filter"></div>
          <div className="fa fa-exchange"></div>
          <div><input type="text" placeholder="הקלד בשביל לחפש" name="search" required /></div>
        
        </div>

          <div>
        {props.patients.map((patient) => (
          <Patient
            key={patient.pk}
            data={patient}
            changeCurrPatient={changeCurrPatient}
          />
        ))}
        
        {Object.keys(currPatient).length === 0 ? null : (
          <CurrPatientDetails selectedPatient={currPatient} />
        )}

        
      </div>
    </div>
    </div>
  );
};

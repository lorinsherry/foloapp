import Axios from "axios";

const $axios = Axios.create({
  baseURL: "/api/",
  headers: {
    "Content-Type": "application/json",
  },
});

export class FoloAppService {
  static getAllMeetings() {
    return $axios.get("get_all_meeting_history/").then((response) => {
      return response.data;
    });
  }

  static getAllPatients() {
    return $axios.get("get_all_patient_list/").then((response) => {
      return response.data;
    });
  }

  static getAllMeetings_by_id_user(id_user) {
    return $axios
      .get(`get_all_meeting_by_id_user/get/${id_user}`)
      .then((response) => {
        return response.data;
      });
  }

  // .get(`get_all_meeting_by_id_user/get/${id_user}`)

  static addMeeting(
    id,
    data,
    descripsion_missiondata,
    instruction,
    report,
    id_user
  ) {
    return $axios
      .post("add_meeting", {
        id,
        data,
        descripsion_missiondata,
        instruction,
        report,
        id_user,
      })
      .then((response) => response.data);
  }
}

import React, {useState, useEffect} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {Welcome} from './components/Welcome';
import {Login} from './components/Login';
import {MeetingHistory} from './components/MeetingHistory/MeetingHistory'
import {PatientsList} from './components/PatientsList/PatientsList'
import {AddPatient} from './components/PatientsList/AddPatient';
import 'bootstrap/dist/css/bootstrap.min.css';
import {FoloAppService} from "./service/api";

function App() {

    const [meetings, setMeetings] = useState([]);
    const [patients, setPatients] = useState([]);


    useEffect(()=>{
        // FoloAppService.getAllMeetings()
        // .then(response => {
        //     const data = JSON.parse(response);
        //     setMeetings(data);
        // });

        FoloAppService.getAllPatients()
        .then(response => {
            const data = JSON.parse(response);
            setPatients(data);
        });
    },[])
    

        
    return (
        <div className="App">
            <Router>
                <Switch>
                    <Route exact path="/" component={Welcome}/>
                    <Route exact path="/Login" component={Login}/>
                    <Route path="/MeetingHistory/:id" >
                        <MeetingHistory meetings={meetings}/>
                         </Route>
                    <Route path="/PatientsList" >
                        <PatientsList patients={patients}/>
                    </Route>
                    <Route path="/AddPatient" />
                    
                </Switch>
            </Router>
        </div>
    );
}

export default App;

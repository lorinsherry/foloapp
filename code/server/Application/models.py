from django.db import models


class Patient(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50,default='Null')
    age = models.IntegerField()
    gender = models.CharField(max_length=10, default='No')
    image_icon = models.CharField(max_length=300, default='No')
    name_contact_person = models.CharField(max_length=50, default='No contact person')
    contact_person = models.IntegerField()


class Meeting(models.Model):
    id = models.AutoField(primary_key=True)
    meeting_date = models.DateTimeField(auto_now_add=True)
    description_mission = models.CharField(max_length=1000, default='No description')
    instruction = models.CharField(max_length=300, default='No instruction')
    report = models.BooleanField(default=False, verbose_name="Done")
    id_user = models.ManyToManyField(Patient,related_name='patients')

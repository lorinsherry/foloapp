from django.urls import path

from . import views

api_prefix = 'api'

urlpatterns = [
    path(f'{api_prefix}/get_all_meeting_history/', views.get_all_meeting_history),
    path(f'{api_prefix}/get_meeting/get/<int:meetingid>,<int:meetingdata>', views.get_meeting),

    path(f'{api_prefix}/get_all_patient_list/', views.get_all_patient_list),
    path(f'{api_prefix}/get_patient/get/<int:patientid>,<int:patientdata>', views.get_patient),

    path(f'{api_prefix}/get_all_meeting_by_id_user/get/<int:id_user>', views.get_all_meeting_by_id_user),
    

    path(f'{api_prefix}/add_meeting/', views.add_meeting),

]

from rest_framework import serializers

from .models import Meeting
from .models import Patient


class MeetingSerializer(serializers.ModelSerializer):
    class Meta:

        model = Meeting
        fields = ['id', 'meeting_date', 'description_mission', 'instruction', 'report','id_user']

    class Meta:
        model = Patient
        fields = ['id', 'name', 'age', 'gender', 'image_icon', 'name_contact_person', 'contact_person']

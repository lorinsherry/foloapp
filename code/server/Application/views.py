from django.core import serializers
from django.http import HttpResponse
from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
import json
from django.core import serializers

from .models import Meeting, Patient


def index(request):
    return HttpResponse("Hello, world. You're at the FoloApp index.")


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_all_meeting_history(request):
    res = serializers.serialize('json', Meeting.objects.all())
    return Response(res, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_all_patient_list(request):
    res = serializers.serialize('json', Patient.objects.all())
    return Response(res, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_meeting(request, meetingid, meetingdata):
    print(meetingid, meetingdata)
    meeting = Meeting.objects.get(id=meetingid, data=meetingdata)
    return Response(Meeting.id, Meeting.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_patient(request, patientid, patientdata):
    print(patientid, patientdata)
    patient = Patient.objects.get(id=patientid, data=patientdata)
    return Response(Patient.id, Patient.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_all_meeting_by_id_user(request, id_user):
    # print(id_user)
    all_meetings = serializers.serialize('json', Meeting.objects.all())
    json_all_meetings_res = json.loads(all_meetings)
    meetings_of_user = []
    id_user_array = []
    # id_user_array.append(id_user)
    for meeting in json_all_meetings_res:
        if meeting['fields']['id_user'] == [id_user]:
            meetings_of_user.append(meeting)
    return Response(meetings_of_user, status=status.HTTP_200_OK)

# **************************

@api_view(['POST'])
@renderer_classes([JSONRenderer])
def add_meeting(request):
    data = request.data
    new_meeting = Meeting(id=data['id'], data=data['data'], descripsion_missiondata=data['descripsion_missiondata'],
                          instruction=data['instruction'], report=data['report'], id_user=data['id_user'])
    new_meeting.save()
    return Response("added", status=status.HTTP_200_OK)

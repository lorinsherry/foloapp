CREATE DATABASE IF NOT EXISTS `foloapp`;

DROP USER IF EXISTS 'foloapp_admin'@'%';
-- password MD5('foloapp_admin')
CREATE USER 'foloapp_admin'@'%' IDENTIFIED WITH mysql_native_password BY '34d5c41a0f42a45ee55e696fe76aef5d';
GRANT ALL ON foloapp.* TO 'foloapp_admin'@'%';
FLUSH PRIVILEGES;
